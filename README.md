# SANDI UAT Issues Tracking

This repository keeps updaing for tracking issues during UAT period. Colleagues from both ASTRI and Silverhorn can file/update/close issues.

### How to create new issue?

A: Click ["Issues"](https://gitlab.com/ypliang/sandi/issues) on the sidebar, click "New issue", then input the detail information of the issue and submit.

* Leave the assignee as "Unassigned" if you don't know whom should be assgined to
* Please give a label to the issue
* You can attach screen shot for better describing the issue

